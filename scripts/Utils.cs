﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

public class Utils
{    
    /// <summary>
    /// Возвращает значение параметра из строки url
    /// </summary>
    /// <param name="urlString"></param>
    /// <param name="paramName"></param>
    /// <returns></returns>
    public static string GetParamValueFromUrl(string urlString, string paramName)
    {
        Uri myUri = new Uri(urlString);
        foreach (string item in myUri.Query.Split('&'))
        {
            string[] parts = item.Replace("?", "").Split('=');
            if (parts[0] == paramName)
            {
                return parts[1];
            }
        }
        return "";
    }
}
