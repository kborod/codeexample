﻿using System;
using TMPro;
using UnityEngine;

public class PopupController : MonoBehaviour
{
    [SerializeField]
    private RectTransform windowRectTransform;

    [SerializeField]
    private Transform btnsPanel;

    [SerializeField]
    private TextMeshProUGUI tf;

    [SerializeField]
    private GameObject btnPrefab;

    private Action<bool> callback = null;
    private Action<bool,object> callback2 = null;
    private Action callback3 = null;
    private object param = null;

    private void Start()
    {
    }

    //Простой попап с кнопкой ок
    public void ShowPopup(string text)
    {
        tf.text = text;
        updateWindowSize();
        addBtn("OK", remove);
    }

    //Попап с вопросом и кнопками ок/отмена. На коллбек приходит результат.
    public void ShowPopup(string text, Action<bool> callback)
    {
        this.callback = callback;
        tf.text = text;
        updateWindowSize();
        addBtn("OK", onOkClicked);
        addBtn("CANCEL", onCancelClicked);
    }

    //Попап с вопросом и кнопками ок/отмена, с передачей параметра. На коллбек приходит результат.
    public void ShowPopup(string text, Action<bool, object> callback, object param)
    {
        this.callback2 = callback;
        this.param = param;
        tf.text = text;
        updateWindowSize();
        addBtn("OK", onOkClicked);
        addBtn("CANCEL", onCancelClicked);
    }

    //Попап с вопросом и кнопками ок/отмена, у которых настраивается тайтл, с передачей параметра. На коллбек приходит результат.
    public void ShowPopup(string text, Action<bool, object> callback, object param, string btnoOkTitle, string btnCancelTitle)
    {
        this.callback2 = callback;
        this.param = param;
        tf.text = text;
        updateWindowSize();
        addBtn(btnoOkTitle, onOkClicked);
        addBtn(btnCancelTitle, onCancelClicked);
    }

    //Блокирующий попап без кнопок
    public void ShowPopupWithoutBtns(string text)
    {
        tf.text = text;
        updateWindowSize();
    }

    public void ShowPopupWithOneBtnAndCallback(string text, string btnTitle, Action callback)
    {
        tf.text = text;
        updateWindowSize();
        this.callback3 = callback;
        addBtn(btnTitle, onOkClicked);
    }

    private void addBtn(string btnText, Action callBack)
    {
        GameObject instance = (GameObject)Instantiate(btnPrefab, btnsPanel);
        instance.GetComponent<BtnPopupController>().Setup(btnText, callBack);
    }


    private void onOkClicked()
    {
        if (callback != null) callback(true);
        if (callback2 != null) callback2(true, param);
        if (callback3 != null) callback3();
        remove();
    }

    private void onCancelClicked()
    {
        if (callback != null) callback(false);
        if (callback2 != null) callback2(false, param);
        remove();
    }

    private void updateWindowSize()
    {
        if (tf.text.Length < 30) windowRectTransform.sizeDelta = new Vector2(400, 250);
        else  windowRectTransform.sizeDelta = new Vector2(500, 300);
    }



    private void remove()
    {
        Destroy(gameObject);
    }


}
