﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupSendRequest : MonoBehaviour
{

    [SerializeField]
    private Transform btnsPanel;

    [SerializeField]
    private TextMeshProUGUI tfTitle;

    [SerializeField]
    private GameObject btnPrefab;

    [SerializeField]
    private TMP_InputField inputField;

    [SerializeField]
    private TextMeshProUGUI tfPlaceHolder;

    private void Awake()
    {
    }

    private void Start()
    {
        addBtn("OK", onOkClicked);
        addBtn("CANCEL", onCancelClicked);
    }

    private void updateStaticTF()
    {
    }

    private void addBtn(string btnText, Action callBack)
    {
        GameObject instance = (GameObject)Instantiate(btnPrefab, btnsPanel);
        instance.GetComponent<BtnPopupController>().Setup(btnText, callBack);
    }


    private void onOkClicked()
    {
        if (inputField.text.Length == 0) return;
        Managers.NetManager.SendStringOnServer(inputField.text, false);
    }

    private void onCancelClicked()
    {
        remove();
    }



    private void remove()
    {
        Destroy(gameObject);
    }


}
