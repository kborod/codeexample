﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BtnPopupController : MonoBehaviour {

    [SerializeField]
    private TextMeshProUGUI tf;

    private Action callback;

    private Action<int> callback2;
    private int btnIndex;

    public void Setup(string text, Action callback)
    {
        tf.text = text;
        this.callback = callback;
        gameObject.GetComponent<Button>().onClick.AddListener(onClickListener);
    }

    public void SetupWithIndex(string text, Action<int> callback, int btnIndex)
    {
        tf.text = text;
        this.callback2 = callback;
        this.btnIndex = btnIndex;
        gameObject.GetComponent<Button>().onClick.AddListener(onClickListener);
    }

    private void onClickListener()
    {
        if (callback != null) callback();
        if (callback2 != null) callback2(btnIndex);
    }

    private void OnDestroy()
    {
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
    }
}
