﻿using UnityEngine;

public class AppManager : MonoBehaviour, IGameManager {

    public EnumManagerStatus status { get; private set; }
    

    public void Startup()
    {
        status = EnumManagerStatus.Started;
        Debug.Log("AppManager started");
    }
}