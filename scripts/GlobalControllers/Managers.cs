﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Managers : MonoBehaviour
{
    public static AppManager AppManager { get; private set; }
    public static ScreenManager ScreenManager { get; private set; }
    public static NetManager NetManager { get; private set; }
    public static TimeManager TimeManager { get; private set; }

    private List<IGameManager> managersList;

    public static bool isAllManagersStarted = false;

    private void Awake()
    {
        //Ставим по дефолту инвариантный CultureInfo (не зависящий от языка юзера), чтобы не было глюков с парсингом (например чтоб не было float -> string где-то разделитель точка, а где-то запятая)
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
        
        
        GameObject go = GameObject.Find("App");
        if (go == null)
        {
            Debug.Log("ERROR: App not found. START WITH PRELOAD SCENE!!!");
            return;
        }

        ScreenManager = go.GetComponent<ScreenManager>();
        AppManager = go.GetComponent<AppManager>();
        NetManager = go.GetComponent<NetManager>();
        TimeManager = go.GetComponent<TimeManager>();

        managersList = new List<IGameManager>();
        managersList.Add(ScreenManager);
        managersList.Add(AppManager);
        managersList.Add(NetManager);
        managersList.Add(TimeManager);

        StartCoroutine(StartupManagers());
    }

    private IEnumerator StartupManagers()
    {
        IGameManager currStartingManager = null;
        int currStartingManagerIndex = 0;
        int managersCount = managersList.Count;

        currStartingManager = managersList[currStartingManagerIndex];
        currStartingManager.Startup();

        yield return null;

        while (true)
        {
            if (currStartingManager.status == EnumManagerStatus.Started)
            {
                currStartingManagerIndex++;
                Debug.Log("StartupProgress:" + currStartingManagerIndex + "/" + managersCount + " started");
                if (currStartingManagerIndex >= managersCount)
                {
                    //все менеджеры стартовали
                    break;
                }
                currStartingManager = managersList[currStartingManagerIndex];
                currStartingManager.Startup();
            }
            yield return null;
        }


        isAllManagersStarted = true;

        Managers.ScreenManager.ShowMainMenu();
    }


    private static int debugMsgMaxPartLength = 16000;
    public static void ToLog(string text)
    {
        int partNum = 0;
        int subStrLength = 0;
        bool complete = false;
        while(complete == false)
        {
            subStrLength = text.Length - debugMsgMaxPartLength * partNum;
            if (subStrLength > debugMsgMaxPartLength) subStrLength = debugMsgMaxPartLength;
            else complete = true;

            Debug.Log(text.Substring(debugMsgMaxPartLength * partNum, subStrLength));
                
            partNum++;
        }
    }
}
