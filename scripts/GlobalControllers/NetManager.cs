﻿using System;
using System.Collections;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetManager : MonoBehaviour, IGameManager {

    public EnumManagerStatus status { get; private set; }
    private WebSocket webSocket;
    private ResponseProcessor responseProcessor;

    private long lastRequestSendDate = 0;

    public void Startup()
    {
        responseProcessor = new ResponseProcessor();
        StartCoroutine(init());
    }

    private IEnumerator init()
    {
        yield return StartCoroutine(openSocket());

        status = EnumManagerStatus.Started;
        Debug.Log("NetManager started");
    }

    private IEnumerator openSocket()
    {
        string url = Config.GameplayWebSocketUrl;
        Debug.Log("Opening connection to " + url);
        webSocket = new WebSocket(new Uri(url));
        yield return StartCoroutine(webSocket.Connect());
        StartCoroutine(readSocketCoroutine());
    }

    private IEnumerator readSocketCoroutine()
    {
        while (true)
        {
            if (webSocket.error != null)
            {
                Debug.Log("NetManager ERROR: " + webSocket.error);
                showConnectionErrorPopup();
                break;
            }

            // обрабатываем за 1 update больше 1 сообщения, но не более максимума
            // (лучше так т.к. очередь может накопиться, особенно с 1 update / sec в фоне webgl)
            const int maxProcessibleMessagesPerFrame = 100;
            for (int i = 0; i < maxProcessibleMessagesPerFrame; i++)
            {
                string message = webSocket.RecvString();
                if (message == null) break;
                onMsgReceived(message);
            }

            yield return 0;
        }
        webSocket.Close();
    }

    private void showConnectionErrorPopup()
    {
        Managers.ScreenManager.ShowPopup("Connection error");
    }

    private void onMsgReceived(string msg)
    {
        Managers.ToLog("[" + Managers.TimeManager.GetServerTimeDate().ToString("HH:mm:ss") + "]_Received: " + msg);
        responseProcessor.OnMessageReceived(msg);
    }

    public void SendRequestOnServer(RequestBase request)
    {
        Debug.Log("SendRequestOnServer:" + request.type);
        if (request is SyncTimeRequest == false) lastRequestSendDate = Managers.TimeManager.GetServerTime();
        SendStringOnServer(JsonConvert.SerializeObject(request), false);
    }

    public void SendStringOnServer(string requestStr, bool needSig)
    {
        ReqToServer r = new ReqToServer();
        r.sig = "";
        r.req = requestStr;
        string rStr = JsonUtility.ToJson(r);
        rStr = "size:" + rStr.Length + ";" + rStr;

        Debug.Log("[" + Managers.TimeManager.GetServerTimeDate().ToString("HH:mm:ss") + "]_" + "SendStringOnServer: " + rStr);

        webSocket.SendString(rStr);
    }

    /// <summary>
    /// Загрузка изображения в объект RawImage по URL.
    /// 
    /// Если RawImage уничтожится до окончания загрузки - текстура не будет присвоена в RawImage
    /// (при уничтожении внешнего объекта, который содержит RawImage).
    /// Сделано в корутине данного класса т.к. он не уничтожается, иначе если бы было на другом объекте
    /// и тот уничтожался - корутина не доходила бы до конца и не очищала бы WWW.Dispose().
    /// 
    /// todo Загружаемая картинка растягивается чтобы заполнить всю rawImage,
    /// todo соответственно не-квадратные картинки меняют пропорции
    /// todo если нужно пофиксить - надо добавить uvRect коррекцию у RawImage
    /// </summary>
    public void LoadImageToRawImage(RawImage rawImage, string url)
    {
        StartCoroutine(ActualLoadImageToRawImage(rawImage, url));
    }

    /// <summary>
    /// Корутина реальной загрузки текстуры по URL и вставки ее в RawImage
    /// </summary>
    private IEnumerator ActualLoadImageToRawImage(RawImage rawImage, string url)
    {
        Debug.Log("LoadImageToRawImage url: " + url);
        WWW www = new WWW(url);

        //todo для теста, удалить и раскоментить перд строку
        /*WWW www = null;
        if (Managers.SocialManager.GetSocialUserId() == "290831")
        {
            www = new WWW("https://vk.com/images/camera_50.png");
        }
        else
        {
            www = new WWW(url);
        }*/
        //
            
        yield return www;

        if (www.error == null)
        {
            if (!rawImage.IsDestroyed())
            {
#if UNITY_ANDROID && !UNITY_EDITOR
                TextureFormat textureFormat = TextureFormat.ETC2_RGB;
#else
                TextureFormat textureFormat = TextureFormat.DXT1;
#endif
                Texture2D texture = new Texture2D(4, 4, textureFormat, false);
                www.LoadImageIntoTexture(texture);
                rawImage.texture = texture;
            }
        }
        else
        {
            Debug.Log("LoadImageToRawImage error: " + www.error);
        }

        www.Dispose();
    }

    public void CloseWebSocketConnection()
    {
        if (webSocket != null) webSocket.Close();
        Debug.Log("WebSocket connection closed");
    }

    public long GetLastRequestSendDate()
    {
        return lastRequestSendDate;
    }


    
    /// <summary>
    /// Загрузка байтового массива на сервер через PUT запрос
    /// </summary>
    public void UploadBytesToServer(string url, byte[] byteArray, Action<bool, string> uploadCallBack)
    {
        StartCoroutine(uploadBytesToServerCoroutine(url, byteArray, uploadCallBack));
    }

    private IEnumerator uploadBytesToServerCoroutine(string uploadUrl, byte[] byteArray, Action<bool, string> uploadBytesToServerCallBack)
    {
        UnityWebRequest www = UnityWebRequest.Put(uploadUrl, byteArray);

        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
            if (uploadBytesToServerCallBack != null) uploadBytesToServerCallBack.Invoke(false, www.error);
        }
        else
        {
            Debug.Log("Upload complete!");
            Debug.Log("uploadBytesToServer Request Response: " + www.downloadHandler.text);
            if (uploadBytesToServerCallBack != null) uploadBytesToServerCallBack.Invoke(true, www.downloadHandler.text);
        }

        www.Dispose();
    }

    private Action<string> getWebRequestCallback = null;
    public void SendGetWebRequest(string url, Action<string> callback)
    {
        StartCoroutine(GetRequest(url));
    }

    private IEnumerator GetRequest(string uri)
    {
        UnityWebRequest www = UnityWebRequest.Get(uri);
        
        yield return www.Send();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("www request complete:Received: " + www.downloadHandler.text);
            if (getWebRequestCallback != null) getWebRequestCallback.Invoke(www.downloadHandler.text);
        }

        www.Dispose();
    }
}