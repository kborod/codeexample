﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour, IGameManager
{
    public EnumManagerStatus status { get; private set; }

    private long nextResyncTime = long.MaxValue;
    private long resyncPeriod = 1000 * 60 * 5;

    private bool synchronizing = true;

    private long maxLag = 3000;
    private int totalAttemps = 0;
    private long startRequestTime;

    private long deltaServerLocalTime = 0;

    private long timeToContinue = 0;



    private void Update()
    {
        if (nextResyncTime < GetServerTime() && synchronizing == false)
        {
            nextResyncTime = GetServerTime() + resyncPeriod;
            syncronizeWithServer();
        }
    }


    public void Startup()
    {
        StartCoroutine(init());
    }

    private IEnumerator init()
    {
        while(true)
        {
            if (Managers.NetManager.status == EnumManagerStatus.Started) break;
            yield return 0;
        }
        syncronizeWithServer();
        while(true)
        {
            if (synchronizing == false) break;
            yield return 0;
        }
        status = EnumManagerStatus.Started;
        nextResyncTime = GetServerTime() + resyncPeriod;
        Debug.Log("TimeManager started");
    }

    private void syncronizeWithServer()
    {
        synchronizing = true;
        startRequestTime = getTimeFromStartGame();
        Managers.NetManager.SendRequestOnServer(new SyncTimeRequest());
    }

    public void OnServerTimeRecieved(long inServerTime)
    {
        long inLag = (getTimeFromStartGame() - startRequestTime) / 2;
        if (inLag > maxLag)
        {
            if (totalAttemps > 3)
            {
                Debug.Log("Internet connection is too slow");
            }
            syncronizeWithServer();
            return;
        }
        if (deltaServerLocalTime == 0)
        {
            //первая синхронизация
            deltaServerLocalTime = (inServerTime + inLag) - getTimeFromStartGame();
        }
        else
        {
            long newDeltaServerLocalTime = (inServerTime + inLag) - getTimeFromStartGame();
            if (newDeltaServerLocalTime < deltaServerLocalTime)
            {
                //локальный таймер торопится. т.е. время по факту в будущем. Назад не мотаем, делаем паузу, чтоб выровнять
                timeToContinue = GetServerTime();
            }
            else
            {
                //локальный таймер отстает
            }
            Debug.Log("Time synchronized.AdjustMS = " + (newDeltaServerLocalTime - deltaServerLocalTime));

            deltaServerLocalTime = newDeltaServerLocalTime;
        }
        
        totalAttemps = 0;
        synchronizing = false;

        Debug.Log("Time synchronized.DELTA = " + deltaServerLocalTime);
    }
    
    public long getTimeFromStartGame()
    {
        return (long)(Time.unscaledTime * 1000);
    }

    public long GetServerTime()
    {
        long res = getTimeFromStartGame() + deltaServerLocalTime;
        if (timeToContinue > res) return timeToContinue;
        return res;
    }

    public DateTime GetServerTimeDate()
    {
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(GetServerTime() / 1000).ToLocalTime();
        return dtDateTime;
    }
}
