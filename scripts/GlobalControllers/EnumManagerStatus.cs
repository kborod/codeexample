﻿public enum EnumManagerStatus
{
    Shutdown,
    Initializing,
    Started
}