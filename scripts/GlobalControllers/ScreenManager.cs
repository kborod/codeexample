﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour, IGameManager {
    
    public EnumManagerStatus status  { get; private set;}
    [SerializeField]
    private Text tfLog;
    [SerializeField]
    private GameObject popupsLayer;
    [SerializeField]
    private GameObject waitScreen;

    [SerializeField]
    private GameObject mainMenuWindow;
    [SerializeField]
    private GameObject newNoticeWindow;

    [SerializeField]
    private GameObject popupPrefab;
    [SerializeField]
    private GameObject popupSendRequestPrefab;






    public void Startup()
    {
        Application.logMessageReceivedThreaded += HandleLog;

        status = EnumManagerStatus.Started;
        Debug.Log("ScreenManager started");
    }

    public void ShowWaitScreen()
    {
        waitScreen.SetActive(true);
    }

    public void RemoveWaitScreen()
    {
        waitScreen.SetActive(false);
    }

    

    //Попап с одной кнопкой ок
    public void ShowPopup(string text)
    {
        GameObject popup = (GameObject)Instantiate(popupPrefab, popupsLayer.transform);
        popup.GetComponent<PopupController>().ShowPopup(text);
    }

    //Попап с вопросом. Две кнопки ок/отмена
    public void ShowPopup(string text, Action<bool> callback, bool aboveBlockLayer = false, bool setAsFirstSibling = true)
    {     
        GameObject popup = (GameObject)Instantiate(popupPrefab, popupsLayer.transform);
        popup.GetComponent<PopupController>().ShowPopup(text, callback);
    }

    //Попап с вопросом. Две кнопки ок/отмена, с передачей параметра
    public void ShowPopup(string text, Action<bool, object> callback, object param)
    {
        GameObject popup = (GameObject)Instantiate(popupPrefab, popupsLayer.transform);
        popup.GetComponent<PopupController>().ShowPopup(text, callback, param);
    }
    //Попап с вопросом. Две кнопки ок/отмена, с передачей параметра, с изменяемыми тайтлами 
    public void ShowPopup(string text, Action<bool, object> callback, object param, string btnOkTitle, string btnCancelTitle)
    {
        GameObject popup = (GameObject)Instantiate(popupPrefab, popupsLayer.transform);
        popup.GetComponent<PopupController>().ShowPopup(text, callback, param, btnOkTitle, btnCancelTitle);
    }
    
    //Попап с одной кнопкой и коллбеком
    public void ShowPopupWithOneBtnAndCallback(string text, string btnTitle, Action callback)
    {
        GameObject popup = (GameObject)Instantiate(popupPrefab, popupsLayer.transform);
        popup.GetComponent<PopupController>().ShowPopupWithOneBtnAndCallback(text, btnTitle, callback);
    }
    
    public void ShowSendRequestPopup()
    {
        GameObject popup = (GameObject)Instantiate(popupSendRequestPrefab, popupsLayer.transform);
    }



    //Показать главное окно
    public void ShowMainMenu()
    {
        mainMenuWindow.gameObject.SetActive(true);
    }

    public void ShowNewNoticeWindow()
    {
        newNoticeWindow.gameObject.SetActive(true);
    }


    void HandleLog(string logString, string stackTrace, LogType type)
    {
        tfLog.text += logString + "\n";
    }

}