﻿public interface IGameManager
{
    EnumManagerStatus status { get; }
    void Startup();
}
