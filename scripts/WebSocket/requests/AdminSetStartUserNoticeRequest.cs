﻿
using System.Collections.Generic;

public class AdminSetStartUserNoticeRequest : RequestBase
{
    public Dictionary<EnumLanguage, string> notice;
    public AdminSetStartUserNoticeRequest(Dictionary<EnumLanguage, string> texts)
    {
        type = "adminSetStartUserNotice";

        this.notice = texts;
    }
}
