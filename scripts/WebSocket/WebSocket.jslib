var LibraryWebSockets = {
$webSocketInstances: [],

SocketCreate: function(url)
{
	var str = Pointer_stringify(url);
	var socket = {
		socket: new WebSocket(str),
		error: null,
		messages: []
	}

	socket.socket.binaryType = 'arraybuffer';

	// type is determined by sender, we should support text and binary
	socket.socket.onmessage = function (e) {
		if (e.data instanceof Blob)
		{
			var reader = new FileReader();
			reader.addEventListener("loadend", function() {
				var array = new Uint8Array(reader.result);
				socket.messages.push(array);
			});
			reader.readAsArrayBuffer(e.data);
		}
		else if (e.data instanceof ArrayBuffer)
		{
			var array = new Uint8Array(e.data);
			socket.messages.push(array);
		}
		else if (typeof e.data === "string")
		{
			var reader = new FileReader();
			reader.addEventListener("loadend", function() {
				var array = new Uint8Array(reader.result);
				socket.messages.push(array);
			});
			var blob = new Blob([e.data]);
			reader.readAsArrayBuffer(blob);
		}
		else
		{
			socket.error = "Unexpected type of data in onmessage: " + Object.prototype.toString.call(e.data);
		}
	};

	socket.socket.onclose = function (e) {
		if (e.code != 1000)
		{
			if (e.reason != null && e.reason.length > 0)
				socket.error = e.reason;
			else
			{
				switch (e.code)
				{
					case 1001: 
						socket.error = "Endpoint going away.";
						break;
					case 1002: 
						socket.error = "Protocol error.";
						break;
					case 1003: 
						socket.error = "Unsupported message.";
						break;
					case 1005: 
						socket.error = "No status.";
						break;
					case 1006: 
						socket.error = "Abnormal disconnection.";
						break;
					case 1009: 
						socket.error = "Data frame too large.";
						break;
					default:
						socket.error = "Error "+e.code;
				}
			}
		}
	}
	var instance = webSocketInstances.push(socket) - 1;
	return instance;
},

SocketState: function (socketInstance)
{
	var socket = webSocketInstances[socketInstance];
	return socket.socket.readyState;
},

SocketError: function (socketInstance, ptr, bufsize)
{
 	var socket = webSocketInstances[socketInstance];
 	if (socket.error == null) return 0;

	stringToUTF8(socket.error, ptr, bufsize);
	return 1;
},

SocketSend: function (socketInstance, ptr, length)
{
	var socket = webSocketInstances[socketInstance];
	socket.socket.send (HEAPU8.buffer.slice(ptr, ptr+length));
},

SocketRecvLength: function(socketInstance)
{
	var socket = webSocketInstances[socketInstance];
	if (socket.messages.length == 0) return 0;
	return socket.messages[0].length;
},

// [ptr] - C# byte buffer
// [bufsize] - buffer size
// returns: <= 0 - on error, actualLength - on complete
SocketRecv: function (socketInstance, ptr, bufsize)
{
	var socket = webSocketInstances[socketInstance];
	if (socket.messages.length == 0) return -1;
	
	var actualLength = socket.messages[0].length;
	if (actualLength == 0) return 0;
	if (actualLength > bufsize) return -2;
	
	HEAPU8.set(socket.messages[0], ptr);
	socket.messages = socket.messages.slice(1);
	return actualLength;
},

SocketClose: function (socketInstance)
{
	var socket = webSocketInstances[socketInstance];
	socket.socket.close();
}
};

autoAddDeps(LibraryWebSockets, '$webSocketInstances');
mergeInto(LibraryManager.library, LibraryWebSockets);
