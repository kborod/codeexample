﻿using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;

public class ResponseProcessor
{
    public void OnMessageReceived(string message)
    {
        ResponseBase rType = JsonUtility.FromJson<SyncTimeResponse>(message);
        switch (rType.type)
        {
            case "desyncResponse":
                processDesyncResponse(JsonConvert.DeserializeObject<DesyncResponse>(message));
                break;
            case "syncTimeResponse":
                processSyncTimeResponse(JsonConvert.DeserializeObject<SyncTimeResponse>(message));
                break;
            default:

                break;
        }
    }

    private void processSyncTimeResponse(SyncTimeResponse response)
    {
        Managers.TimeManager.OnServerTimeRecieved(response.serverTime);
    }

    private void processDesyncResponse(DesyncResponse response)
    {
        Managers.ScreenManager.ShowPopup("Server error:" + response.message);
    }

    private void log(string msg)
    {
        Debug.Log("ResponseProcessor." + msg);
    }
}
