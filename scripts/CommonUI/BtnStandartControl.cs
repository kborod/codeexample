﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BtnStandartControl : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI tfTitle;

    private Action callback;
    private Action<object> callback2;
    private object parameter;

    private void Start()
    {
        EnableListener();
    }

    public void Setup(string title, Action callback)
    {
        this.tfTitle.text = title;
        this.callback = callback;
    }

    public void Setup(string title, Action<object> callback, object parameter)
    {
        this.tfTitle.text = title;
        this.callback2 = callback;
        this.parameter = parameter;
    }

    private void OnDestroy()
    {
        RemoveListener();
    }

    private void onClickListener()
    {
        if (callback != null) callback();
        if (callback2 != null) callback2(parameter);
    }

    public void SetInteractable(bool value)
    {
        gameObject.GetComponent<Button>().interactable = value;
    }

    public void EnableListener()
    {
        RemoveListener();
        gameObject.GetComponent<Button>().onClick.AddListener(onClickListener);
    }

    public void RemoveListener()
    {
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
    }
}

