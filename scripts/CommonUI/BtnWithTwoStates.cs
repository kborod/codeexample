﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BtnWithTwoStates : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI tfTitle;

    [SerializeField]
    private Sprite enabledSprite;
    [SerializeField]
    private Sprite disabledSprite;

    private Action callback;
    private Action<object> callback2;
    private object parameter;

    private void Start()
    {
        EnableListener();
    }

    public void Setup(string title, Action callback)
    {
        if (tfTitle != null) this.tfTitle.text = title;
        if (this.callback == null) this.callback = callback;
    }

    public void Setup(string title, Action<object> callback, object parameter)
    {
        if (tfTitle != null) this.tfTitle.text = title;
        if (this.callback2 == null) this.callback2 = callback;
        this.parameter = parameter;
    }

    private void OnDestroy()
    {
        RemoveListener();
    }

    private void onClickListener()
    {
        if (callback != null) callback();
        if (callback2 != null) callback2(parameter);
    }

    public void SetInteractable(bool value)
    {
        gameObject.GetComponent<Button>().interactable = value;
    }

    //Два состояния / енаблед и дисаблед. В обоих состояниях работает клик
    public void SetEnabled(bool value)
    {
        GetComponent<Image>().sprite = (value == true) ? enabledSprite : disabledSprite;
    }

    public void EnableListener()
    {
        RemoveListener();
        gameObject.GetComponent<Button>().onClick.AddListener(onClickListener);
    }

    public void RemoveListener()
    {
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
    }
}

