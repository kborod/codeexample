﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Кнопка текстовая с двумя состояниями
/// </summary>
public class BtnStandartTextWithTwoStates : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI tfTitle;

    private Action<object> callback;

    private Color32 enabledColor;
    private FontStyles enabledFontStyles;
    private Color32 disabledColor;
    private FontStyles disabledFontStyles;

    private object parameter;

    public void Setup(string title, Action<object> callback, Color32 enabledColor, FontStyles enabledFontStyles, Color32 disabledColor, FontStyles disabledFontStyles, object parameter)
    {
        if (tfTitle != null) this.tfTitle.text = title;
        if (this.callback == null) gameObject.GetComponent<Button>().onClick.AddListener(onClickListener);
        this.callback = callback;
        this.enabledColor = enabledColor;
        this.enabledFontStyles = enabledFontStyles;
        this.disabledColor = disabledColor;
        this.disabledFontStyles = disabledFontStyles;
        this.parameter = parameter;
    }

    private void OnDestroy()
    {
        gameObject.GetComponent<Button>().onClick.RemoveListener(onClickListener);
    }

    private void onClickListener()
    {
        if (callback != null) callback(parameter);
    }

    public void SetInteractable(bool value)
    {
        gameObject.GetComponent<Button>().interactable = value;
    }

    //Два состояния / енаблед и дисаблед. В обоих состояниях работает клик
    public void SetEnabled(bool value)
    {
        if (value == true)
        {
            tfTitle.color = new Color32(enabledColor.r, enabledColor.g, enabledColor.b, enabledColor.a);
            tfTitle.fontStyle = enabledFontStyles;
        }
        else
        {
            tfTitle.color = new Color32(disabledColor.r, disabledColor.g, disabledColor.b, disabledColor.a);
            tfTitle.fontStyle = disabledFontStyles;
        }
    }
}

