﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuControl : MonoBehaviour
{
    [SerializeField]
    private Button btnNewNotice;

    [SerializeField]
    private Button btnSendCommand;
    
    void Start()
    {
        btnNewNotice.onClick.AddListener(() =>
        {
            Managers.ScreenManager.ShowNewNoticeWindow();
            remove();
        });

        btnSendCommand.onClick.AddListener(() =>
        {
            Managers.ScreenManager.ShowSendRequestPopup();
        });
    }

    private void remove()
    {
        gameObject.SetActive(false);

    }
}
