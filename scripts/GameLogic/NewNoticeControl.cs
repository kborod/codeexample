﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class NewNoticeControl : MonoBehaviour
{
    [SerializeField]
    private Button btnClose;

    [SerializeField]
    private InputField inputRus;

    [SerializeField]
    private InputField inputEng;

    [SerializeField]
    private Button btnSend;
    
    void Start()
    {
        btnClose.onClick.AddListener(() => remove());
        btnSend.onClick.AddListener(() => Managers.ScreenManager.ShowPopup("Realy send new notice?", onBtnSendPopup));
    }

    private void onBtnSendPopup(bool res)
    {
        if (res == false) return;


        Dictionary<EnumLanguage, string> texts = new Dictionary<EnumLanguage, string>();
        //texts.Add(EnumLanguage.rus, Utils.getTextForSaveInDB(inputRus.text));
        //texts.Add(EnumLanguage.eng, Utils.getTextForSaveInDB(inputEng.text));
        texts.Add(EnumLanguage.rus, inputRus.text);
        texts.Add(EnumLanguage.eng, inputEng.text);
        
        Managers.NetManager.SendRequestOnServer(new AdminSetStartUserNoticeRequest(texts));
    }

    private void remove()
    {
        gameObject.SetActive(false);
        Managers.ScreenManager.ShowMainMenu();
    }
}
